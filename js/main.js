$.getJSON('http://localhost/json/names.json', function(data) {
	//генерация таблицы
	$.each(data.person, function(i, f) {
		var tRow = $("<tr>")
		tRow.append("<td>" + f.firstName);
		tRow.append("<td>" + f.secondName);
		tRow.append("<td>" + f.job);
		tRow.append("<td>" + f.city);
		$("#userdata").append(tRow)
	});
	//подсветка строк
	$("#userdata tbody tr:even").addClass("userdata1");
	$("#userdata tbody tr:odd").addClass("userdata2");
	$("#userdata tbody tr").hover(
			function(){$(this).toggleClass("userdata3")},
			function(){$(this).toggleClass("userdata3")});
	//добавление ссылок
	$("#userdata tbody tr").attr("href",
			"details.html");
	$("#userdata tbody tr").click(function(event) {
		event.preventDefault();
		window.open($(this).attr("href"),
			       "popupWindow", 
			       "width=600,height=600,scrollbars=yes" );
	});
});
